# General Usage
## Installing
Please just use load unpacked - the other options don't really work
## Using
Just click the OwO-ify button at the bottom in the middle of (almost) every page. (Even most TXT documents)
# Implementation
If you want to make your website automatically trigger the OwO, add this to your HTML code:

```html
<div id="owo_autoenabled"></div>
```
(NOTE: It doesn't need to be a div - it just requires the id `autoowo_enabled`)
This will make it auto owo - only works if the extension is installed


To just get the version, just add
```html
<div id="owo_version"></div>
```

(NOTE: For both of these elements, it replaces the content - for version, it replaces it with the version. For auto enabled, it replaces it with `OwO-Auto.Enabled.True___Version.[version]` and hides it)
# Documentation
## Functions
### getAllTextNodes()
  Gets all text nodes and returns them
### confirmation(bypassconf)
  Checks for value in "sites" variable if it is in there at all. Then asks depending on the level
  bypassconf is used to let OwO make it return yes anyway (so i dont need to bother writing another if to prevent it from asking when the site loads it)
### OwO(bypassconf):
  Makes page be OwO-ified - runs confirmation(bypassconf) in the process
### More Soon
(Sadly not usable within browser, thats why i added the special things in #Implementation)
