const sites = {
    "broken": [
      "www.w3schools.com"
    ],
    "working": [
      "cyber-hub.net",
      "discordapp.com"
    ],
    "notrecomended": [
      "developer.mozilla.org",
      "gitlab.com",
      "github.com"
    ],
    "decent": [
      "www.youtube.com"
    ]
}
const messages = {
    "broken": "This site has been marked as Broken by 0J3#9971 (the creator of this extension)\nProceed with OwOing Anyway?\n\nOwO",
    "decent": "This site is partially broken (main features still work) when OwOing - proceed?",
    "unknown": "This site doesn't yet have a verified status. If this site breaks, Reload the page!"
}
const config = {
  autoowo_id: "owo_autoenabled",
  version_id: "owo_version",
  version: "1.0.1"
}
function getAllTextNodes(){
  var result = [];

  (function scanSubTree(node){
    if(node.childNodes.length)
      for(var i = 0; i < node.childNodes.length; i++)
        scanSubTree(node.childNodes[i]);
    else if(node.nodeType == Node.TEXT_NODE)
      result.push(node);
  })(document);

  return result;
}
function quote(str){
  return (str+'').replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
}
function confirmation(bypassconf) {
  if (bypassconf == true) {
    return "yes"
  } else if (sites.broken.includes(document.location.host)) {
    var confirmopen = window.confirm(messages.broken)
    if (confirmopen == true) {
      return "yes"
    } else {
      return "no"
    }
  } else if (sites.decent.includes(document.location.host)) {
      var confirmopen = window.confirm(messages.decent)
      if (confirmopen == true) {
        return "yes"
      } else {
        return "no"
      }
    } else if (sites.working.includes(document.location.host)) {
      return "yes"
    } else if (sites.notrecomended.includes(document.location.host)) {
      alert("This wesbite works, but it is not reccomended to use this extension on it. Refresh if you want to cancel")
      return "yes"
    } else {
      alert(messages.unknown)
      return "yes"
    }
 }
function owo(bypassconf) {
  if (confirmation(bypassconf) == "yes") {
    var faces = ["(・`ω´・)", ";;w;;", "owo", "UwU", ">w<", "^w^"];
    console.log("Loaded Faces")
    console.log(faces)
    getAllTextNodes().forEach(function(node){
      var e = node.nodeValue
      e = e.replace(/(?:r|l)/g, "w");
      e = e.replace(/(?:R|L)/g, "W");
      e = e.replace(/n([aeiou])/g, 'ny$1');
      e = e.replace(/N([aeiou])/g, 'Ny$1');
      e = e.replace(/N([AEIOU])/g, 'Ny$1');
      e = e.replace(/ove/g, "uv");
      e = e.replace(/\!+/g, " " + faces[Math.floor(Math.random() * faces.length)] + " ");
      node.nodeValue = e
    });
    try {
      document.getElementById('OwOButton').id = 'hiddenOwOButton'
    } catch (e) {
      document.getElementById('hiddenOwOButton').id = 'hiddenOwOButton'
    }
  }
}
var btn = document.createElement("BUTTON");
btn.class = "OwOButton"
btn.id = "OwOButton"
btn.innerHTML = "OwO-ify"
btn.onclick = function(){owo(false)};
document.body.appendChild(btn);


// Allow website to auto-OwO
if (document.getElementById(config.autoowo_id)) {
  owo(true)
  document.getElementById(config.autoowo_id).innerHTML = "OwO-Auto.Enabled.True___Version." + config.version
  document.getElementById(config.autoowo_id).style.display = "none"
  siiimpleToast.alert("This website automatically enabled OwO-mode.")
}
if (document.getElementById(config.version_id)) {
  document.getElementById(config.version_id).innerHTML = config.version
}
// document.addEventListener('DOMContentLoaded', (event) => {
//   if (document.getElementById(config.autoowo_id)) {
//   }
// })
